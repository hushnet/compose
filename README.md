# compose - hush.rip
This repository contains the `docker-compose.yaml` file used to instantiate and
configure all of this project's services. Should be straightforward to get it
running but some manual interaction is required.

## Requisites
- *nix shell
- Git
- Docker

# Table of contents
- [Setup](#setup)
  1. [Clone the project](#clone-the-project)
  2. [Add secret files](#add-secret-files)
  3. [Run setup script](#run-setup-script)
  4. [Configure Openfire](#configure-openfire)
- [Scripts](#scripts)
  - [setup.sh](#setup-sh)
  - [_openfire.sh](#openfire-sh)
  - [update.sh](#update-sh)
  - [reset.sh](#reset-sh)
- [Debugging web interface](#debug-web)

# Other pages
- [Openfire](md/openfire.md)

# Setup <a name="setup"></a>
## 1. Clone the project <a name="clone-the-project"></a>
```
git clone https://gitlab.com/hushnet/compose.git
mkdir secrets
```

## 2. Add secret files <a name="add-secret-files"></a>
This should be self-explanatory, but the files you put in this directory are
confidential. Secrets should be created outside of the `compose` directory, as
`setup.sh` will copy them into `compose`. 
```
root
|-- compose
    |-- ...
|-- secrets
    |-- openldap-passwd
    |-- postgres-passwd
    |-- web-admin.pub
```
- `openldap-passwd`: Bind password for OpenLDAP admin.
- `postgres-passwd`: PostgreSQL database password.
- `web-admin.pub`: Web admin's public PGP key.

After the project runs successfully for the first time, `secrets` will also
contain a directory called `nginx`. This directory contains the SSL certificates
obtained from Let'sEncrypt certbot. Make a backup of this, certbot will rate
limit aggressively and you will be locked out of obtaining a new certificate for
an entire week.

## 3. Run setup script <a name="run-setup-script"></a>
```
cd ..
sh script/setup.sh
```
Read the 'Scripts' section below for information about `setup.sh` and other
available scripts.

## 4. Configure Openfire <a name="configure-openfire"></a>
In your browser, to the Openfire admin panel running on port `9090`. Unless
specified below, use the default selection for all settings.
### 4.1) Server settings
- XMPP Domain Name: `hush.rip`
- Server Host Name (FQDN): `hush.rip`
### 4.2) Database settings
- Choose: `Standard Database Connection`
### 4.3) Database Settings - Standard Connection
- Database Driver Presets: `PostgreSQL`
- Database URL: `jdbc:postgresql://postgres:5432/hushdb`
- Username: `hushdb_user`
- Password: Copy from `secrets/postgres-passwd`
### 4.4) Profile Settings
- Choose: `Directory Server (LDAP)`
### 4.5) Profile Settings: Connection Settings
- Server type: `OpenLDAP`
- Protocol: `ldap`
- Host: `openldap`
- Port: `1389`
- Base DN: `dc=hush,dc=rip`
- Administrator DN: `cn=admin,dc=hush,dc=rip`
- Password: Copy from `secrets/openldap-passwd`
### 4.6) Profile Settings: User Mapping
- Name: `{uid}`
### 4.7) Administrator Account
- Add administrator: `user01`

Openfire setup is now complete, but you should create a new LDAP user to replace
`user01` as the admin as soon as you can.

# Scripts <a name="scripts"></a>
Scripts are located in `compose/scripts`. Make sure you understand what the
script does before you run it, as it's possible to accidentally delete important
data with these scripts.

## `setup.sh` <a name="setup-sh"></a>
Finalizes the portion of the setup process that can be done through a shell.
Manual interaction is still required during and after this script.
1. Copies `/root/secrets` into `/root/compose`.
2. Runs the project and waits for certbot to obtain the SSL certificate.
3. Copies the certificate files into the Openfire container.
4. Runs `_openfire.sh` in an interactive shell.

## `_openfire.sh` <a name="openfire-sh"></a>
Configures Openfire for SSL/TLS.
1. Prompts for current keystore password.
2. Performs system upgrade.
3. Downloads Let'sEncrypt certificate authority file.
4. Converts certbot PEM certificate files to PKCS12 format.
5. Inserts converted files into the keystore.
6. Restarts the `openfire` service.

## `update.sh` <a name="update-sh"></a>
Updates containers that pull from remote repositories. This must be run to apply
changes made to the `web` repository. Also pulls changes to the `compose`
repository.
1. Stops running containers.
2. Deletes container images that need to be updated.
3. Performs a git reset and clean.
4. Pulls changes to `compose` repository.
5. Rebuilds and restarts.

## `reset.sh` <a name="reset-sh"></a>
⚠️ Warning: This script will destroy important data.
1. Stops running containers.
2. Wipes all containers.
3. Wipes all volumes.

# Debugging web interface <a name="debug-web"></a>
`docker-compose.yaml` contains commented instructions for how to:
- Switch web interface build context: Allows you to build from a local clone of
  the `web` repo.
- Expose web interface debug port: Allows you to bypass nginx and connect
  directly to the web interface server.