# Openfire - hush.rip
See also:
[Openfire homepage](https://www.igniterealtime.org/projects/openfire/index.jsp),
[Docker container](https://hub.docker.com/r/nasqueron/openfire)

Initial setup is described in [README.md](../readme.md).

# Plugins
## HTTP File Upload
- Config: `Server > Server Settings > HTTP Binding`
- Description: Allows clients to share files, as described in the XEP-0363
  'HTTP File Upload' specification.
- Author: Guus der Kinderen

## MotD (Message of the Day)
- Config: `Users/Groups > Users > MotD Properties`
- Description: Allows admins to have a message sent to users each time they log
  in.
- Author: Ryan Graham

## Push Notification
- Config: `Server >  Server Manager > System Properties`
- Description: Adds Push Notification (XEP-0357) support to Openfire.
- Author: Guus der Kinderen

## Search
- Config: `Users/Groups > Users > Advanced User Search`
- Description: Provides support for Jabber Search. (XEP-0055)
- Author: Ryan Graham

## Subscription
- Config: `Server > Server Settings > Subscription Properties`
- Description: Automatically accepts or rejects subscription requests.
- Author: Ryan Graham
