version: "3.9"
# https://docs.docker.com/compose/compose-file

networks:
    hushnet:
        driver: bridge

volumes:
    web-data-protection-keys:
    web-user-public-keys:
    web-user-applications:
    postgres-data:
    openldap-data:
    openfire-data:


services:
    hush_nginx:
        image: jonasal/nginx-certbot
        hostname: nginx
        networks:
            hushnet:
                aliases:
                    - nginx
        ports:
            - 80:80 # HTTP
            - 443:443 # HTTPS
        environment:
            - CERTBOT_EMAIL=hushdotrip@proton.me
        volumes:
            - ./secrets/nginx:/etc/letsencrypt
            - ./data/nginx:/etc/nginx/user_conf.d:ro

    hush_web:
        build: https://gitlab.com/hushnet/web.git#main
        # ! DEBUG: Switch build context to build from local repo
        # build: ../hush_web
        hostname: hushweb
        networks:
            hushnet:
                aliases:
                    - hushweb
        # ! DEBUG: Expose port 5000 to bypass nginx
        ports:
            - 5000:5000
        volumes:
            - web-data-protection-keys:/app/keys
            - web-user-public-keys:/app/user-public-keys
            - web-user-applications:/app/user-applications
            - ./secrets/web-admin.pub:/app/admin/keys/web-admin.pub:ro
            - ./secrets/openldap-passwd:/app/admin/secrets/openldap-passwd:ro

    hush_postgres:
        image: postgres
        restart: always
        hostname: postgres
        networks:
            hushnet:
                aliases:
                    - postgres
        ports:
            - 5432:5432
        env_file:
            - env/postgres.env
        volumes:
            - postgres-data:/var/lib/postgresql/data
            - ./secrets/postgres-passwd:/run/secrets/postgres-passwd:ro

    hush_openldap:
        image: bitnami/openldap
        restart: always
        hostname: openldap
        networks:
            hushnet:
                aliases:
                    - openldap
        ports:
            - 1389:1389 # Plain
            - 1636:1636 # Encrypted
        env_file:
            - env/openldap.env
        volumes:
            - openldap-data:/bitnami/openldap
            - ./secrets/openldap-passwd:/run/secrets/openldap-passwd:ro

    hush_openfire:
        image: nasqueron/openfire
        restart: always
        hostname: openfire
        networks:
            hushnet:
                aliases:
                    - openfire
        ports:
            - 9090:9090 # Admin panel
            - 5222:5222 # Plaintext (with StartTLS)
            - 5223:5223 # Encrypted (legacy)
            - 7777:7777 # File transfer
            - 7070:7070 # HTTP Binding
            - 7443:7443 # HTTP Binding (SSL)
        volumes:
            - openfire-data:/var/lib/openfire
