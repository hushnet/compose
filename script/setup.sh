#!/bin/sh

# Container names
C_OPENFIRE="compose-hush_openfire-1"

# Directories
D_CERT_ROOT="/root/compose/secrets/nginx/live/hush.rip"
D_CERT_OPENFIRE="/var/lib/openfire/conf/security"

# Functions
copy_cert() {
    docker cp -L "${D_CERT_ROOT}/fullchain.pem" "${1}:${2}/fullchain.pem"
    docker cp -L "${D_CERT_ROOT}/chain.pem" "${1}:${2}/chain.pem"
    docker cp -L "${D_CERT_ROOT}/cert.pem" "${1}:${2}/cert.pem"
    docker cp -L "${D_CERT_ROOT}/privkey.pem" "${1}:${2}/privkey.pem"
}

##### MAIN LOGIC #####

cd /root/compose

# Copy secrets from /root into the project
if [ -d /root/secrets ]; then
    if [ ! -d /root/compose/secrets ]; then
        cp -r /root/secrets /root/compose/secrets
    fi
else
    exit
fi

# Run and wait for certbot to get our cert
docker compose up -d
echo "..."
sleep 45

# Setup Openfire
copy_cert "$C_OPENFIRE" "$D_CERT_OPENFIRE"
docker cp /root/compose/script/_openfire.sh "${C_OPENFIRE}:/"
docker exec -it "$C_OPENFIRE" sh "/_openfire.sh"

docker compose restart