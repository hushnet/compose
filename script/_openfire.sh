#!/bin/sh

# Read keystore password
stty -echo
printf "Openfire keystore password: "
read PASSWORD
stty echo
printf "\n"

# Update everything
apt update
apt upgrade -y
apt --fix-broken install -y

# Download LetsEncrypt authority file
cd /var/lib/openfire/conf/security
wget -o cafile.pem "https://letsencrypt.org/certs/letsencryptauthorityx1.pem"

# Convert PEMs to PKCS12
openssl pkcs12 -export \
    -in fullchain.pem \
    -inkey privkey.pem \
    -CAfile cafile.pem \
    -out certificate.p12 \
    -name hush.rip \
    -password "pass:${PASSWORD}"

# Import PKCS12s
keytool -importkeystore \
    -destkeystore keystore \
    -srckeystore certificate.p12 \
    -srcstoretype pkcs12 \
    -deststoretype jks \
    -alias hush.rip \
    -srcstorepass "$PASSWORD" \
    -deststorepass "$PASSWORD" \
    -noprompt

# Restart Openfire
service openfire stop
service openfire start
