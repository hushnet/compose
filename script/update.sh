#!/bin/sh

# Go offline and remove web image so it pulls from repo
printf "...\nGoing offline in 5 seconds.\n...\n"
sleep 5
docker compose down
docker image rm compose_hush_web

# Pull changes from compose repo
printf "...\nGit: Hard reset and clean incoming in 5 seconds.\n...\n"
sleep 5
git reset --hard
git clean -fd
git pull

# Go back online
docker compose up -d